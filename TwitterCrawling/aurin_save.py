#!/usr/bin/python
# -*- coding: utf-8 -*-
import os

import couchdb
import json
import sys
from pprint import pprint

# CouchDB
# Create tweet_store database
try:
    db_url = ''
    if not os.path.exists('../config.conf'):
        sys.exit(1)
    with open('../config.conf') as f:
        config = f.readline()
        db_url = config.split('=')[1].strip()
    db_name_income = "aurin_income"
    db_name_sports = "aurin_sports"
    db_server = couchdb.Server(url=db_url)
    try:
        db_income = db_server.create(db_name_income)
        db_sports=db_server.create(db_name_sports)
    except couchdb.http.PreconditionFailed:
        db_income = db_server[db_name_income]
        db_sports = db_server[db_name_sports]

    # Save Social Welfare Data
    with open('./AURIN/SA2_Income_Support.json', encoding="utf-8") as file_income:
        for line in file_income:
            json_income = json.loads(line)["features"]
            for i in range(len(json_income)):
                id = json_income[i]["properties"]["area_name"]
                db_income[id] = json_income[i]["properties"]
    file_income.close()

    # Save Sports Location Data
    with open('./AURIN/Victoria_Sport_and_Recreation_Facility_Locations_2015-2016.json', encoding="utf-8") as file_sports:
        for line in file_sports:
            json_sports = json.loads(line)["features"]
            for i in range(len(json_sports)):
                db_sports.save(json_sports[i])
    file_sports.close()

except:
    print("Unexpected error:", sys.exc_info()[0])
    raise
