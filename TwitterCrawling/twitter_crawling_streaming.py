#!/usr/bin/python
# -*- coding: utf-8 -*-
import os

from tweepy import OAuthHandler
from tweepy import StreamListener
from tweepy import Stream
from tweepy.utils import import_simplejson
import couchdb
import sys

# CouchDB
# Create tweet_store database
try:
    db_url = ''
    if not os.path.exists('../config.conf'):
        sys.exit(1)
    with open('../config.conf') as f:
        config = f.readline()
        db_url = config.split('=')[1].strip()
    db_list = ['sydney_no_repeat', 'melbourne_no_repeat', 'brisbane_no_repeat', 'perth_no_repeat', 'adelaide_no_repeat',
               'gold_coast_no_repeat', 'newcastle_no_repeat', 'canberra_no_repeat', 'wollongong_no_repeat',
               'sunshine_coast_no_repeat', 'hobart_no_repeat', 'geelong_no_repeat', 'townsville_no_repeat', 'cairns_no_repeat',
               'toowoomba_no_repeat', 'darwin_no_repeat', 'ballarat_no_repeat']
    city_list = ['Sydney', 'Melbourne', 'Brisbane', 'Perth', 'Adelaide', 'Gold Coast', 'Newcastle', 'Canberra', 'Wollongong',
                 'Sunshine Coast', 'Hobart', 'Geelong', 'Townsville', 'Cairns', 'Toowoomba', 'Darwin', 'Ballarat']
    db_name = []
    db_server = couchdb.Server(url=db_url)
    for i in range(len(db_list)):
        try:
            db_name.append(db_server.create(db_list[i]))
        except couchdb.http.PreconditionFailed:
            db_name.append(db_server[db_list[i]])
        # db_name.append(db_server[db_list[i]])

    # Twitter
    # user credentials to access Twitter API
    access_token1 = "808915178607800321-9yYVjKywGxH7zmebqD2dpAhmwXFtePY"
    access_token_secret1 = "FtnOOi6c3eUVmPpnY5DhVpDJYUFBm8U5mg5Orr3JgoBHP"
    consumer_key1 = "eVNidFC2Xdje4DZ56Fgcdzhn6"
    consumer_secret1 = "DBrHHvNHUfkxaFlRXzzFlV1KkRnT3SSK0beGdiemZeok0jkp27"
    access_token2 = "3045377717-r7I7yXqgRGZOrJX8E5W8suracWXzyulnGpTYI3W"
    access_token_secret2 = "SbUC0bDEeQA3pWzx6E3Qlj2VHVUv2k941pEJDRezu0348"
    consumer_key2 = "2EaGFjCr5IJBgdg5TENj3wS9u"
    consumer_secret2 = "nFCk7He3xImObeWCrpoIpCRTDps3lTOm8AmUlAdZzWBIWyTvXj"


    # use OAuth interface as entry point
    new_auth1 = OAuthHandler(consumer_key1, consumer_secret1)
    new_auth1.set_access_token(access_token1, access_token_secret1)
    new_auth2 = OAuthHandler(consumer_key2, consumer_secret2)
    new_auth2.set_access_token(access_token2, access_token_secret2)

    # Stream listening
    class StreamListener(StreamListener):

        def on_data(self, data):
            tweet = import_simplejson().loads(data)
            if "place" in tweet.keys():
                for city in city_list:
                    if tweet["place"] is not None and "full_name" in tweet["place"].keys() and city in str(tweet["place"]["full_name"]).split(", "):
                        # db_name[city_list.index(city)].save(tweet)
                        try:
                            db_name[city_list.index(city)][tweet["id_str"]] = tweet
                        except couchdb.http.ResourceConflict:
                            continue
                        # print("FullName: " + city)
                        return True
                    elif "user" in tweet.keys() and tweet["user"] is not None and "location" in tweet["user"].keys() and city in str(tweet["user"]["location"]).split(", "):
                        # db_name[city_list.index(city)].save(tweet)
                        try:
                            db_name[city_list.index(city)][tweet["id_str"]] = tweet
                        except couchdb.http.ResourceConflict:
                            continue
                        # print("Location:" + city)
                        return True

            return True

        def on_error(self, status):
            print(status)
            return False

    if __name__ == '__main__':
        listener = StreamListener()
        streamer1 = Stream(auth=new_auth1, listener=listener)
        streamer2 = Stream(auth=new_auth1, listener=listener)
        # Filter Australian Tweets
        streamer1.filter(locations=[111.5,-44.84,155.45,-11.86])
        streamer2.filter(locations=[111.5, -44.84, 155.45, -11.86])
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise
