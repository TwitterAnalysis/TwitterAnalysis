# The readme file for Assignment 2 - Team 15

# Purpose
This is a cloud-based system for Australian City Analysis.

# Folder Structure:
/TiwtterAnalysis
    /SentimentAPI
    /ServerDeployment
    /TopicCluster
    /TwitterCrawling
    /TwitterRestAPICrawler
    /web

# Introduction for each folder:
1. SentimentAPI: twitter sentimental analysis based on time, event and aurin data.
2. ServerDeployment: ansible playbook files and templates for deployment and initialization
3. TopicCluster: topic cluster and sports analysis.
4. TwitterCrawling: stream crawler.
5. TwtterRestAPICrawler: restful-API crawler.
5. web: front-end templates and restful-API.