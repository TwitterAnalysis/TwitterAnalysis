# -*- coding:utf-8 -*-
import os
import time

import sys
import tweepy
import couchdb
import datetime

db_url = ''
if not os.path.exists('../config.conf'):
    sys.exit(1)
with open('../config.conf') as f:
    config = f.readline()
    db_url = config.split('=')[1].strip()
db_server = couchdb.Server(url=db_url)


consumer_key0 = "H9JyGn63IGHAAfHmbyvTximim"
consumer_secret0 = "hLJbC2cMNrlUnFSdraYjfdc7PjbrtqZzlhwVvF3YAEr8iVPK2N"
access_token0 = "813695056997552128-fDxRSmrVcMqJdmSiMU1BJILWkrd3ILT"
access_token_secret0 = "5G4bzvmNS34kjHaZLePvpMzEW2vKAbkWrlcKUUAUakvQH"

consumer_key1 = "xv1tStmjHytxBOuNDantMfPOh"
consumer_secret1 = "GcNEpn6VqMu6k7neIdWaDLWY0rjc50hm9EzMnqD0ElX9bvJRbu"
access_token1 = "852848959366209536-eTrEHpZBUbkMBX1r1wAVNwq6aBI2Jeu"
access_token_secret1 = "ovSro9TFIutqMQQ622b4gi6DudHqPvXPGbqO5Y08MEMaL"

consumer_key2="VxSyY3RiZYMzZso4ss0r9fZ2g"
consumer_secret2="vq4M8Y5eAqlAwK4Q1sulc8Vsh5PhKgHwVdo4wSQIaZQytbeikB"
access_token2="	832995895876595712-XA0OupWAAUqUEcevC2abxshkmoDRjc3"
access_token_secret2="atchHB0oI3RIAVBM2QpyUcd3m4ZYKMpQceVPg2kv4meyF"

auth0 = tweepy.OAuthHandler(consumer_key0, consumer_secret0)
auth0.set_access_token(access_token0, access_token_secret0)
auth1 = tweepy.OAuthHandler(consumer_key1, consumer_secret1)
auth1.set_access_token(access_token1, access_token_secret1)
auth2=tweepy.OAuthHandler(consumer_key2,consumer_secret2)
auth2.set_access_token(access_token2,access_token_secret2)
city_list = ['Sydney', 'Melbourne', 'Brisbane', 'Perth', 'Adelaide', 'Gold Coast', 'Newcastle', 'Canberra', 'Wollongong',
             'Sunshine Coast', 'Hobart', 'Geelong', 'Townsville', 'Cairns', 'Toowoomba', 'Darwin', 'Ballarat']

geo_info= {'sydney': (-33.87, 151.21,'0073b76548e5984f'), 'melbourne': (-37.81, 144.96,'01864a8a64df9dc4'), 'brisbane': (-27.46, 153.02,'004ec16c62325149'), 'perth': (-31.96, 115.84,'0118c71c0ed41109'),
           'adelaide': (-34.93, 138.6,'01e8a1a140ccdc5c'), 'newcastle': (-32.92, 151.75,'01cc83b303f0c068'), 'gold_coast': (-28.07, 153.44,'017453ae077eafd3'), 'canberra': (-35.31, 149.13,'01e4b0c84959d430'),
           'wollongong': (-34.42, 150.87,'0180d6313aa616b2'), 'sunshine_coast': (-25.88, 152.56,'7e347291be7bbab4'), 'hobart': (-42.85, 147.29,'019e32e73d7d3282'), 'geelong': (-38.14, 144.32,'01552cf31a0d108e'),
           'townsville': (-19.26, 146.78,'0013cae44d65aff9'), 'cairns': (-16.92, 145.75,'00c262dc7a56fb4e'), 'darwin': (-12.43, 130.85,'002891d33ed032d3'), 'toowoomba': (-27.56, 151.96,'01f97e1230f068f8'),
           'ballarat': (-37.56, 143.84,'0077e693247f71d8')}
fetched_users=[]
deduplicate_db={}
city_max_ids={}
for k,v in geo_info.items():
    try:
        deduplicate_db[k]=db_server.create((k+'_no_repeat'))
    except couchdb.http.PreconditionFailed:
        deduplicate_db[k] = db_server[(k + '_no_repeat')]
    city_max_ids[k]=0
api0 = tweepy.API(auth0)
api1=tweepy.API(auth1)
api2=tweepy.API(auth2)
apis=[api0, api1,api2]
maxID=0
currentAPIIndex=0
logName= 'crawling_log_%s.log' % (datetime.datetime.strftime(datetime.datetime.today(), '%Y_%m_%d_%H_%M_%S'))
f = open(logName, 'w+')
f.write('start to crawl\n')
f.close()
def getTimeStr():
    return datetime.datetime.strftime(datetime.datetime.today(), '%Y-%m-%d %H:%M:%S')
def append_log(message):
    f=open(logName,'a')
    f.write(getTimeStr()+' - '+message+'\n')
    f.close()

while(True):
    for k,v in geo_info.items():
        ids = []
        try:
            if city_max_ids[k]==0:
                results = apis[currentAPIIndex % 3].search(q='place:%s' % v[2], count=100)
            else:
                results = apis[currentAPIIndex % 3].search(q='place:%s' % v[2], count=100, since_id=city_max_ids[k])
            for result in results:
                try:
                    deduplicate_db[k][result.id_str] = result._json
                except couchdb.http.ResourceConflict:
                    continue
                ids.append(result.id)
                if result.user.id not in fetched_users:
                    for status in tweepy.Cursor(apis[currentAPIIndex%3].user_timeline,id=result.user.id).items(10):
                        if status.place and status.place.name in city_list and status.place.name.lower().replace(' ', '_') in geo_info.keys():
                            try:
                                deduplicate_db[status.place.name.lower().replace(' ', '_')][status.id_str] = status._json
                            except couchdb.http.ResourceConflict:
                                continue
                            time.sleep(1)
                    fetched_users.append(result.user.id)
            ids.sort(reverse=True)
            if len(ids)>0:
                city_max_ids[k]=ids[0]
            append_log('%d new status at %s'%(len(results),k))
        except tweepy.RateLimitError:
            append_log('handle rate limit exception for api%d start to sleep'%(currentAPIIndex%3))
            time.sleep(60*2)
            append_log('stop sleep, keep crawling')
            if currentAPIIndex > 100000003:
                currentAPIIndex =0
            currentAPIIndex+=1
            if len(fetched_users)>100000000:
                fetched_users=[]

