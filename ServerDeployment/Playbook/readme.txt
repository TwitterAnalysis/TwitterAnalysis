Readme for cluster deployment


++++++++++++++++++++++++++++++++++++++++++++++++
1. install all necessary environment on clusters
 run ansible-playbook 1_env_deployment.yml

2. ***** set username and password on couchdb*****
 open browser and access the couchdb Web-UI, set the username and password for administrator


3. ***** correct template config file for application*****
 change the db_url in config.conf as your couchdb url: http://<username>:<password>@<address>:<port>

4. deploy application on clusters
 run ansible-playbook 2_app_deployment.yml

5. run crawlers
 run ansible-playbook run_havesters.yml

6. run analyzers
 run ansible-playbook run_analysis.yml

7. run web server
 run ansible-playbook run_webserver.yml
+++++++++++++++++++++++++++++++++++++++++++++++