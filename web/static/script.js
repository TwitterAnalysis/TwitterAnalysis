var settings= {
    "cities": {
        "sydney": {
            "lat": -33.86785,
            "lon": 151.20732
        },
        "melbourne": {
            "lat": -37.814,
            "lon": 144.96332
        },
        "brisbane": {
            "lat": -27.46977,
            "lon": 153.02512
        },
        "perth": {
            "lat": -31.95053,
            "lon": 115.86046
        },
        "adelaide": {
            "lat": -34.92850,
            "lon": 138.60075
        },
        "gold_coast": {
            "lat": -28.01667,
            "lon": 153.40000
        },
        "newcastle": {
            "lat": -32.92833,
            "lon": 151.78167
        },
        "canberra": {
            "lat": -35.28094,
            "lon": 149.13001
        },
        "wollongong": {
            "lat": -34.42781,
            "lon": 150.89306
        },
        "sunshine_coast": {
            "lat": -26.65000,
            "lon": 153.06667
        },
        "hobart": {
            "lat": -42.88214,
            "lon": 147.32719
        },
        "geelong": {
            "lat": -38.14992,
            "lon": 144.36172
        },
        "townsville": {
            "lat": -19.25896,
            "lon": 146.81695
        },
        "cairns": {
            "lat": -16.91855,
            "lon": 145.77805
        },
        "toowoomba": {
            "lat": -27.55982,
            "lon": 151.95067
        },
        "darwin": {
            "lat": -12.46344,
            "lon": 130.84564
        },
        "ballarat": {
            "lat": -37.56216,
            "lon": 143.85026
        }
    },
    "length": 17
};
var tabs = ['apiMap', 'apiList', 'apiGraph'];
// var host = "http://115.146.87.119:8080/";//for test
var host = "/"; //for production
var tempData;
var mapMarkers = [];
var nextHalfData = {};
var nextHalfUrl = {
    by_city: host + "sentiment/by_city/",
    by_city_aurin: host + "sentiment/by_city/aurin/",
    by_time: host + "sentiment/by_time/",
    by_weekday: host + "sentiment/by_weekday/",
    by_season: host + "sentiment/by_season/",
    by_event: host + "sentiment/by_event/"
};
var timeIntervalList = [];
function clearAllInterval() {
    while (timeIntervalList.length !== 0) {
        var tid = timeIntervalList.pop();
        clearInterval(tid);
    }
}
$(document).ready(function () {
    $('.navTab').on('click', function (e) {
        $('.nav-tabs').children().removeClass('active');
        $(this).addClass('active').attr("value", function (i, val) {
            $('#' + val).css({
                'display': 'block',
            });
            for (var index in tabs) {
                if (tabs[index] != val) {
                    $('#' + tabs[index]).css({
                        'display': 'none',
                    });
                }
            }
        });
    });
    getClusterResultInit();
    getGeneralWordcloudResultInit();
    getHashtagWordcloudResultInit();
    getLangResultInit();
    getSportsLangInit();
    getSportsSentimentInit();
    loadNextHalfData();
    getSentimentByCityInit();
    getSentimentOfUnemploymentInit();
    getSentimentByTimeInit();
    getSentimentByWeekdayInit();
    getSentimentBySeasonInit();
    getSentimentByEventInit();
});

function loadNextHalfData() {
    var dataPromises = Object.keys(nextHalfUrl).map(function (key) {
        return axios.get(nextHalfUrl[key]).then(function (res) {
            nextHalfData[key] = res.data;
        }).catch(function (error) {
            console.log(error);
        });
    });
}

function getSentimentByCityInit() {
    $('#get_sentiment_by_city').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_by_city').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Sentiment Type</td><td>Number</td></tr></table></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-7"><h3 id="time" style="color:white;padding:0;margin:0"></h></div>' +
            '<div class="col-sm-12 text-center" id="chart" ></div>'
        );
        loadMap();
        drawSentimentByCity();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimentByCity() {
    clearAllInterval();
    $('#time').html('');
    $('#availableDateInterval_G').html('');
    $('#availableDateInterval_G').append('<option value="1">By City</option>');
    $('#availableDateInterval_G').append('<option value="2">By City Positive</option>');
    var cities = Object.keys(nextHalfData.by_city);
    absoluteNumber();

    $('#availableDateInterval_G').on('change', function () {
        var type = this.value;
        if (type == '1') {
            absoluteNumber();
        } else {
            positiveFrequency();
        }
    });
    function absoluteNumber() {
        clearAllInterval();
        $('#time').html('');
        var cities = Object.keys(nextHalfData.by_city);
        var positive = cities.map(function (key) {
            return nextHalfData.by_city[key][0];
        });
        var positiveData = ['positive'].concat(positive);
        var neutral = cities.map(function (key) {
            return nextHalfData.by_city[key][1];
        });
        var neutralData = ['neutral'].concat(neutral);
        var negative = cities.map(function (key) {
            return nextHalfData.by_city[key][2];
        });
        var negativeData = ['negative'].concat(negative);

        draw([positiveData, neutralData, negativeData], "line", cities);
    }

    function positiveFrequency() {
        clearAllInterval();
        $('#time').html('');
        var cities = Object.keys(nextHalfData.by_city);
        var positive = cities.map(function (key) {
            return nextHalfData.by_city[key][0];
        });
        var neutral = cities.map(function (key) {
            return nextHalfData.by_city[key][1];
        });
        var negative = cities.map(function (key) {
            return nextHalfData.by_city[key][2];
        });
        var frequency = calPositive(positive, neutral, negative, cities);
        //        console.log(frequency);
        var frequencyData = ['positivefrequency'].concat(frequency);
        drawPercentage([frequencyData], 'line', cities);
    }

    //add data to list and marker to map
    $('.numDataList').html('<tr><td>City</td><td>Sentiment Type</td><td>Number</td></tr>');
    if (mapMarkers.length !== 0) {
        for (var marker in mapMarkers) {
            mapInstance.removeLayer(mapMarkers[marker]);
        }
        mapMarkers = [];
    }
    for (var i in cities) {
        $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>Positive</td><td>' + nextHalfData.by_city[cities[i]][0] + '</td></tr>');
        $('.numDataList').append('<tr><td></td><td>Neutral</td><td>' + nextHalfData.by_city[cities[i]][1] + '</td></tr>');
        $('.numDataList').append('<tr><td></td><td>Negtive</td><td>' + nextHalfData.by_city[cities[i]][2] + '</td></tr>');
        var oneCircleMarker = L.circleMarker([settings.cities[cities[i]].lat, settings.cities[
            cities[i]].lon], {
            stroke: false,
            fillColor: 'red',
            fillOpacity: 0.7,
            radius: 10,
        });
        var msg = '<h4 class="text-center cusFont">' + cities[i].toUpperCase() + '</h4>';
        msg += '<p>Positive: ' + nextHalfData.by_city[cities[i]][0] + ' posts</p>';
        msg += '<p>Neutral: ' + nextHalfData.by_city[cities[i]][1] + ' posts</p>';
        msg += '<p>Negtive: ' + nextHalfData.by_city[cities[i]][2] + ' posts</p>';
        oneCircleMarker.bindPopup(msg, {
            closeButton: false,
        });
        oneCircleMarker.on('mouseover', function (e) {
            this.openPopup();
        });
        mapInstance.addLayer(oneCircleMarker);
        mapMarkers.push(oneCircleMarker);
    }
}

function getSentimentOfUnemploymentInit() {
    clearAllInterval();
    $('#time').html('');
    $('#get_sentiment_of_unemployment').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_of_unemployment').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Percentage</td></tr></table></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').html(
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        loadMap();
        drawSentimentOfUnemployment();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimentOfUnemployment() {
    clearAllInterval();
    $('#time').html('');
    var cities = Object.keys(nextHalfData.by_city_aurin);
    var percentage = cities.map(function (key) {
        return nextHalfData.by_city_aurin[key];
    });
    var percentageData = ['percentage'].concat(percentage);
    var positive = cities.map(function (key) {
        return nextHalfData.by_city[key][0];
    });
    var neutral = cities.map(function (key) {
        return nextHalfData.by_city[key][1];
    });
    var negative = cities.map(function (key) {
        return nextHalfData.by_city[key][2];
    });
    var frequency = calPositive(positive, neutral, negative, cities);
    //        console.log(frequency);
    var frequencyData = ['positivefrequency'].concat(frequency);
    // draw([percentageData],'bar',cities);

    percentageData.splice(5, 1);
    percentageData.splice(7, 1);
    percentageData.splice(11, 1);

    frequencyData.splice(5, 1);
    frequencyData.splice(7, 1);
    frequencyData.splice(11, 1);

    cities.splice(4, 1);
    cities.splice(6, 1);
    cities.splice(10, 1);
    c3.generate({
        bindto: '#chart',
        data: {
            columns: [percentageData, frequencyData],
            types: {
                percentage: 'line',
                positivefrequency: 'line'
            },
            axes: {
                percentage: 'y',
                positivefrequency: 'y2'
            }
        },
        padding: {
            top: 20,
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: cities,
                tick: {
                    rotate: -55,
                    multiline: false
                },
                height: 70
            },
            y: {
                tick: {
                    format: function (d) {
                        return d + '%';
                    }
                },
                min: 10,
                max: 100
            },
            y2: {
                show: true,
                tick: {
                    format: d3.format('%')
                },
                min: 0.1,
                max: 1
            }

        }
    });

    //add data to list and marker to map
    $('.numDataList').html('<tr><td>City</td><td>Percentage</td></tr>');
    if (mapMarkers.length !== 0) {
        for (var marker in mapMarkers) {
            mapInstance.removeLayer(mapMarkers[marker]);
        }
        mapMarkers = [];
    }
    for (var i in cities) {
        $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>' + nextHalfData.by_city_aurin[cities[i]].toFixed(4) + '%</td></tr>');
        var oneCircleMarker = L.circleMarker([settings.cities[cities[i]].lat, settings.cities[
            cities[i]].lon], {
            stroke: false,
            fillColor: 'red',
            fillOpacity: 0.7,
            radius: 10,
        });
        var msg = '<h4 class="text-center cusFont">' + cities[i].toUpperCase() + '</h4>';
        msg += '<p>Percentage: ' + nextHalfData.by_city_aurin[cities[i]].toFixed(4) + '%</p>';
        oneCircleMarker.bindPopup(msg, {
            closeButton: false,
        });
        oneCircleMarker.on('mouseover', function (e) {
            this.openPopup();
        });
        mapInstance.addLayer(oneCircleMarker);
        mapMarkers.push(oneCircleMarker);
    }
}

function getSentimentByTimeInit() {
    $('#get_sentiment_by_time').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_by_time').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Time</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr></table></div>'
        );
        $('#apiMap').addClass('text-center');
        $('#apiMap').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-7"><h3 id="time" style="color:white;padding:0;margin:0"></h></div>' +
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        drawSentimentByTime();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimentByTime() {
    clearAllInterval();
    $('#time').html('');
    var categoriesTime = ['5am-8am', '8am-11am', '11am-2pm', '2pm-5pm', '5pm-8pm', '8pm-12am', '12am-2am', '2am-5am'];
    var cities = Object.keys(nextHalfData.by_time);
    //add data to list
    $('.numDataList').html('<tr><td>City</td><td>Time</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr>');
    $('#availableDateInterval_G').html('');
    for (var i in cities) {
        for (var j in categoriesTime) {
            if (j === '0') {
                $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>' + categoriesTime[j] + '</td><td>' + nextHalfData.by_time[cities[i]][j][0] + '</td><td>' + nextHalfData.by_time[cities[i]][j][1] + '</td><td>' + nextHalfData.by_time[cities[i]][j][2] + '</td></tr>');
            } else {
                $('.numDataList').append('<tr><td></td><td>' + categoriesTime[j] + '</td><td>' + nextHalfData.by_time[cities[i]][j][0] + '</td><td>' + nextHalfData.by_time[cities[i]][j][1] + '</td><td>' + nextHalfData.by_time[cities[i]][j][2] + '</td></tr>');
            }
        }
    }
    $('#availableDateInterval_G').append('<option value="1">By Time</option>');
    $('#availableDateInterval_G').append('<option value="2">By time Positive</option>');
    $('#availableDateInterval_G').append('<option value="3">By time and cities</option>');
    $('#availableDateInterval_G').append('<option value="4">By time and cities Positive</option>');
    for(var i in cities){
        $('#availableDateInterval_G').append('<option value='+cities[i]+'>'+cities[i]+' Positive</option>');
    }

    byCategoriesTime();

    $('#availableDateInterval_G').on('change', function () {
        var type = this.value;
        if (type == '1') {
            byCategoriesTime();
        } else if (type == '3') {
            byTimeAndCities();
        } else if (type == '2') {
            byTimePositive();
        } else if (type == '4') {
            byTimeAndCitiesPositive();
        } else {
            byTimeForOneCity(type);
        }
    });


    function byTimeForOneCity(city){
        clearAllInterval();
        $('#time').html('');
        console.log(nextHalfData.by_time[city]);
        var cityTime = nextHalfData.by_time[city];
        var cityTimePositive = calPositiveByCities(cityTime);
        var cityTimePositiveData = ['positivefrequency'].concat(cityTimePositive);
        drawPercentageWithGrid([cityTimePositiveData], categoriesTime);

    }
    function byTimePositive() {
        clearAllInterval();
        $('#time').html('');
        var result = categoriesTime.map(function (categoriesTime, index) {
            return getSum(nextHalfData.by_time, index);
        });

        //----------positive------------
        var allPositive = result.map(function (result) {
            return result[0];
        });

        //-------------neutral--------------
        var allneutral = result.map(function (result) {
            return result[1];
        });

        //-------------negative--------------
        var allnegative = result.map(function (result) {
            return result[2];
        });


        var allFrequency = calPositive(allPositive, allneutral, allnegative, categoriesTime);

        var allFrequencyData = ['positivefrequency'].concat(allFrequency);

        drawPercentage([allFrequencyData], "line", categoriesTime);
    }

    function byTimeAndCitiesPositive() {
        clearAllInterval();
        $('#time').html('');
        var posInit = cities.map(function (city) {
            return nextHalfData.by_time[city][0][0];
        });
        var neutInit = cities.map(function (city) {
            return nextHalfData.by_time[city][0][1];
        });
        var negInit = cities.map(function (city) {
            return nextHalfData.by_time[city][0][2];
        });
        var initFrequency = calPositive(posInit, neutInit, negInit, cities);
        var initFrequencyData = ['positivefrequency'].concat(initFrequency);
        var chart_byTime_interval = drawPercentageWithGrid([initFrequencyData], cities);
        // var chart_byTime_interval = drawWithGrid([posInit, neutInit, negInit], cities, 40000);
        var timeIndex = 1;
        document.getElementById('time').innerText = 'Time Period: ' + categoriesTime[0];
        var timeInterval = setInterval(function () {
            if (timeIndex === categoriesTime.length) {
                timeIndex = 0;
            }
            var pos = cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][0];
            });

            var neut = cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][1];
            });

            var neg = cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][2];
            });
            var changedFrequency = calPositive(pos, neut, neg, cities);
            var changedFrequencyData = ['positivefrequency'].concat(changedFrequency);

            chart_byTime_interval.load({
                columns: [changedFrequencyData]
            });

            document.getElementById('time').innerText = 'Time Period: ' + categoriesTime[timeIndex];
            timeIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);
    }

    function byCategoriesTime() {
        clearAllInterval();
        $('#time').html('');
        var result = categoriesTime.map(function (categoriesTime, index) {
            return getSum(nextHalfData.by_time, index);
        });
        //----------positive------------
        var allPositive = result.map(function (result) {
            return result[0];
        });
        var positive_allData = ['positive'].concat(allPositive);
        //-------------neutral--------------
        var allneutral = result.map(function (result) {
            return result[1];
        });
        var neutral_allData = ['neutral'].concat(allneutral);
        //-------------negative--------------
        var allnegative = result.map(function (result) {
            return result[2];
        });
        var negative_allData = ['negative'].concat(allnegative);
        draw([positive_allData, neutral_allData, negative_allData], "line", categoriesTime);
    }

    function byTimeAndCities() {
        clearAllInterval();
        $('#time').html('');
        var posInit = ['positive'].concat(cities.map(function (city) {
            return nextHalfData.by_time[city][0][0];
        }));
        var neutInit = ['neutral'].concat(cities.map(function (city) {
            return nextHalfData.by_time[city][0][1];
        }));
        var negInit = ['negative'].concat(cities.map(function (city) {
            return nextHalfData.by_time[city][0][2];
        }));
        var chart_byTime_interval = drawWithGrid([posInit, neutInit, negInit], cities, 60000);
        var timeIndex = 1;
        document.getElementById('time').innerText = 'Time Period: ' + categoriesTime[0];
        var timeInterval = setInterval(function () {
            if (timeIndex === categoriesTime.length) {
                timeIndex = 0;
            }
            var pos = ['positive'].concat(cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][0];
            }));

            var neut = ['neutral'].concat(cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][1];
            }));

            var neg = ['negative'].concat(cities.map(function (city) {
                return nextHalfData.by_time[city][timeIndex][2];
            }));

            chart_byTime_interval.load({
                columns: [
                    pos, neut, neg
                ]
            });

            document.getElementById('time').innerText = 'Time Period: ' + categoriesTime[timeIndex];
            timeIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);
    }
}

function getSentimentByWeekdayInit() {
    $('#get_sentiment_by_weekday').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_by_weekday').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Day of weeks</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr></table></div>'
        );
        $('#apiMap').addClass('text-center');
        $('#apiMap').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-7"><h3 id="time" style="color:white;padding:0;margin:0"></h></div>' +
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        drawSentimenByWeekday();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimenByWeekday() {
    clearAllInterval();
    $('#time').html('');
    var weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    var cities = Object.keys(nextHalfData.by_weekday);
    //add data to list
    $('.numDataList').html('<tr><td>City</td><td>Day of weeks</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr>');
    $('#availableDateInterval_G').html('');
    for (var i in cities) {
        for (var j in weekdays) {
            if (j === '0') {
                $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>' + weekdays[j] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][0] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][1] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][2] + '</td></tr>');
            } else {
                $('.numDataList').append('<tr><td></td><td>' + weekdays[j] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][0] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][1] + '</td><td>' + nextHalfData.by_weekday[cities[i]][j][2] + '</td></tr>');
            }
        }
    }
    $('#availableDateInterval_G').append('<option value="1">By day of weeks</option>');
    $('#availableDateInterval_G').append('<option value="2">By day of weeks Positive</option>');
    $('#availableDateInterval_G').append('<option value="3">By day of weeks and cities</option>');
    $('#availableDateInterval_G').append('<option value="4">By day of weeks and cities Positive</option>');
    for(var i in cities){
        $('#availableDateInterval_G').append('<option value='+cities[i]+'>'+cities[i]+' Positive</option>');
    }
    byWeekday();

    $('#availableDateInterval_G').on('change', function () {
        var type = this.value;
        if (type == '1') {
            byWeekday();
        } else if(type == '2') {
            byWeekdayPositive();
        } else if(type == '3') {
            byWeekdayAndCities();
        } else if(type == '4'){
            byWeekdaysAndCitiesPositive();
        } else {
            byWeekForOneCity(type);
        }
    });
    function byWeekdayPositive() {
        clearAllInterval();
        $('#time').html('');
        var result = weekdays.map(function (weekdays, index) {
            return getSum(nextHalfData.by_weekday, index);
        });

        //----------positive------------
        var allPositive = result.map(function (result) {
            return result[0];
        });

        //-------------neutral--------------
        var allneutral = result.map(function (result) {
            return result[1];
        });

        //-------------negative--------------
        var allnegative = result.map(function (result) {
            return result[2];
        });


        var allFrequency = calPositive(allPositive, allneutral, allnegative, weekdays);

        var allFrequencyData = ['positivefrequency'].concat(allFrequency);

        console.log(allFrequencyData);
        drawPercentage([allFrequencyData], "bar", weekdays);
    }

    function byWeekForOneCity(city){
        clearAllInterval();
        $('#time').html('');
        console.log(nextHalfData.by_weekday[city]);
        var cityWeek = nextHalfData.by_weekday[city];
        var cityWeekPositive = calPositiveByCities(cityWeek);
        var cityWeekPositiveData = ['positivefrequency'].concat(cityWeekPositive);
        drawPercentageWithGrid([cityWeekPositiveData], weekdays);

    }

    function byWeekdaysAndCitiesPositive() {
        clearAllInterval();
        $('#time').html('');

        var posWeekInit = cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][0];
        });
        var neuWeektInit = cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][1];
        });
        var negWeekInit = cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][2];
        });

        var initWeekFrequency = calPositive(posWeekInit,neuWeektInit,negWeekInit,cities);
        var initWeekFrequencyData = ['positivefrequency'].concat(initWeekFrequency);
    //    var chart_byWeekday_interval = drawWithGrid('byWeekday_interval', [posWeekInit, neuWeektInit, negWeekInit], cities, 35000);
        var chart_byWeekday_interval = drawPercentageWithGrid([initWeekFrequencyData],cities);

        var weekIndex = 1;
        document.getElementById('time').innerText = weekdays[0];
        var timeInterval = setInterval(function () {
            if (weekIndex === weekdays.length) {
                weekIndex = 0;
            }

            var pos = cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][0];
            });

            var neut = cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][1];
            });

            var neg = cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][2];
            });

            var changedWeekFrequency = calPositive(pos,neut,neg,cities);
            var changedWeekFrequencyData = ['positivefrequency'].concat(changedWeekFrequency);
            chart_byWeekday_interval.load({
                columns: [changedWeekFrequencyData]
            });
            document.getElementById('time').innerText = weekdays[weekIndex];
            weekIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);

    }
    function byWeekday() {
        clearAllInterval();
        $('#time').html('');
        var week_result = weekdays.map(function (weekdays, index) {
            return getSum(nextHalfData.by_weekday, index);
        });
        //----------positive------------
        var weekPositive = week_result.map(function (week_result) {
            return week_result[0];
        });
        var positive_weekData = ['positive'].concat(weekPositive);
        //-------------neutral--------------
        var weekneutral = week_result.map(function (week_result) {
            return week_result[1];
        });
        var neutral_weekData = ['neutral'].concat(weekneutral);
        //-------------negative--------------
        var weeknegative = week_result.map(function (week_result) {
            return week_result[2];
        });
        var negative_weekData = ['negative'].concat(weeknegative);
        draw([positive_weekData, neutral_weekData, negative_weekData], "line", weekdays);
    }

    function byWeekdayAndCities() {
        clearAllInterval();
        $('#time').html('');
        var posWeekInit = ['positive'].concat(cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][0];
        }));
        var neuWeektInit = ['neutral'].concat(cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][1];
        }));
        var negWeekInit = ['negative'].concat(cities.map(function (city) {
            return nextHalfData.by_weekday[city][0][2];
        }));
        var chart_byWeekday_interval = drawWithGrid([posWeekInit, neuWeektInit, negWeekInit], cities, 50000);

        var weekIndex = 1;
        document.getElementById('time').innerText = weekdays[0];
        var timeInterval = setInterval(function () {
            if (weekIndex === weekdays.length) {
                weekIndex = 0;
            }

            var pos = ['positive'].concat(cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][0];
            }));

            var neut = ['neutral'].concat(cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][1];
            }));

            var neg = ['negative'].concat(cities.map(function (city) {
                return nextHalfData.by_weekday[city][weekIndex][2];
            }));

            chart_byWeekday_interval.load({
                columns: [
                    pos, neut, neg
                ]
            });

            document.getElementById('time').innerText = weekdays[weekIndex];
            weekIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);

    }
}

function getSentimentBySeasonInit() {
    $('#get_sentiment_by_season').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_by_season').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Seasons</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr></table></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-7"><h3 id="time" style="color:white;padding:0;margin:0"></h></div>' +
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );

        loadMap();
        drawSentimentBySeason();

        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimentBySeason() {
    clearAllInterval();
    $('#time').html('');
    var cities = Object.keys(nextHalfData.by_season);
    var seasons = ['Spring', 'Summer', 'Autumn', 'Winter'];
    $('#availableDateInterval_G').html('');
    $('#availableDateInterval_G').append('<option value="1">By seasons</option>');
    $('#availableDateInterval_G').append('<option value="2">By seasons Positive</option>');
    bySeason();

    $('#availableDateInterval_G').on('change', function () {
        var type = this.value;
        if (type == '1') {
            bySeason();
        } else {
            bySeasonFrequency();
        }
    });

    function bySeason(){
        var season_result = seasons.map(function (Seasons, index) {
            return getSum(nextHalfData.by_season, index);
        });
        //----------positive------------
        var seasonPositive = season_result.map(function (season_result) {
            return season_result[0];
        });
        var positive_seasonData = ['positive'].concat(seasonPositive);
        //-------------neutral--------------
        var seasonneutral = season_result.map(function (season_result) {
            return season_result[1];
        });
        var neutral_seasonData = ['neutral'].concat(seasonneutral);
        //-------------negative--------------
        var seasonnegative = season_result.map(function (season_result) {
            return season_result[2];
        });
        var negative_seasonData = ['negative'].concat(seasonnegative);

        draw([positive_seasonData, neutral_seasonData, negative_seasonData], "line", seasons);
    }

    function bySeasonFrequency(){
        var season_result = seasons.map(function (Seasons, index) {
            return getSum(nextHalfData.by_season, index);
        });
        //----------positive------------
        var seasonPositive = season_result.map(function (season_result) {
            return season_result[0];
        });
        //-------------neutral--------------
        var seasonneutral = season_result.map(function (season_result) {
            return season_result[1];
        });
        //-------------negative--------------
        var seasonnegative = season_result.map(function (season_result) {
            return season_result[2];
        });

        var seasonPositiveFrequency = calPositive(seasonPositive,seasonneutral,seasonnegative,seasons);
        var seasonPositiveFrequencyData = ['positivefrequency'].concat(seasonPositiveFrequency);
        drawPercentage([seasonPositiveFrequencyData], 'line', seasons);
    }


    //load data to list and marker to map
    $('.numDataList').html('<tr><td>City</td><td>Seasons</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr>');
    for (var i in cities) {
        var msg = '<h4 class="text-center cusFont">' + cities[i].toUpperCase() + '</h4>';
        for (var j in seasons) {
            if (j === '0') {
                $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>' + seasons[j] + '</td><td>' + nextHalfData.by_season[cities[i]][j][0] + '</td><td>' + nextHalfData.by_season[cities[i]][j][1] + '</td><td>' + nextHalfData.by_season[cities[i]][j][2] + '</td></tr>');
            } else {
                $('.numDataList').append('<tr><td></td><td>' + seasons[j] + '</td><td>' + nextHalfData.by_season[cities[i]][j][0] + '</td><td>' + nextHalfData.by_season[cities[i]][j][1] + '</td><td>' + nextHalfData.by_season[cities[i]][j][2] + '</td></tr>');
            }
            msg += '<p>' + seasons[j].toUpperCase() + ' Positive: ' + nextHalfData.by_season[cities[i]][j][0] + ' Neutral: ' + nextHalfData.by_season[cities[i]][j][1] + ' Negative: ' + nextHalfData.by_season[cities[i]][j][2] + '</p>';
        }
        var oneCircleMarker = L.circleMarker([settings.cities[cities[i]].lat, settings.cities[
            cities[i]].lon], {
            stroke: false,
            fillColor: 'red',
            fillOpacity: 0.7,
            radius: 10,
        });
        oneCircleMarker.bindPopup(msg, {
            closeButton: false,
        });
        oneCircleMarker.on('mouseover', function (e) {
            this.openPopup();
        });
        mapInstance.addLayer(oneCircleMarker);
        mapMarkers.push(oneCircleMarker);
    }
}

function getSentimentByEventInit() {
    $('#get_sentiment_by_event').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sentiment_by_event').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>Date</td><td>City</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr></table></div>'
        );
        $('#apiMap').addClass('text-center');
        $('#apiMap').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-7"><h3 id="time" style="color:white;padding:0;margin:0"></h></div>' +
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        drawSentimentByEvent();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function drawSentimentByEvent() {
    clearAllInterval();
    $('#time').html('');
    var days = ['April 11', 'April 12', 'April 13', 'April 14', 'April 15', 'April 16', 'April 17', 'April 18', 'April 19', 'April 20', 'April 21', 'April 22', 'April 23', 'April 24', 'April 25', 'April 26'];
    var cities = Object.keys(nextHalfData.by_event);
    $('.numDataList').html('<tr><td>Date</td><td>City</td><td>Positive</td><td>Neutral</td><td>Nagetive</td></tr>');
    $('#availableDateInterval_G').html('');
    for (var i in days) {
        for (var j in cities) {
            if (j === '0') {
                $('.numDataList').append('<tr><td>' + days[i] + '</td><td>' + cities[j] + '</td><td>' + nextHalfData.by_event[cities[j]][i][0] + '</td><td>' + nextHalfData.by_event[cities[j]][i][1] + '</td><td>' + nextHalfData.by_event[cities[j]][i][2] + '</td></tr>');
            } else {
                $('.numDataList').append('<tr><td></td><td>' + cities[j] + '</td><td>' + nextHalfData.by_event[cities[j]][i][0] + '</td><td>' + nextHalfData.by_event[cities[j]][i][1] + '</td><td>' + nextHalfData.by_event[cities[j]][i][2] + '</td></tr>');
            }
        }
    }
    $('#availableDateInterval_G').append('<option value="1">By Date</option>');
    $('#availableDateInterval_G').append('<option value="2">By date Positive</option>');
    $('#availableDateInterval_G').append('<option value="3">By date and cities</option>');
    $('#availableDateInterval_G').append('<option value="4">By date and cities Positive</option>');
    for(var i in cities){
        $('#availableDateInterval_G').append('<option value='+cities[i]+'>'+cities[i]+' Positive</option>');
    }

    byDate();
    $('#availableDateInterval_G').on('change', function () {
        var type = this.value;
        if (type == '1') {
            byDate();
        } else if (type == '2'){
            byDatePositive();
        } else if (type == '3'){
            byDateAndCities();
        }else if (type == '4'){
            byDateAndCitiesPositive();
        } else {
            byEventForOneCity(type);
        }
    });

    function byEventForOneCity(city){
        clearAllInterval();
        $('#time').html('');
        console.log(nextHalfData.by_event[city]);
        var cityEvent = nextHalfData.by_event[city];
        var cityEventPositive = calPositiveByCities(cityEvent);
        var cityEventPositiveData = ['positivefrequency'].concat(cityEventPositive);
        drawPercentageWithGrid([cityEventPositiveData], days);

    }
    function byDatePositive(){
        clearAllInterval();
        $('#time').html('');
        var days_result = days.map(function (days, index) {
            return getSum(nextHalfData.by_event, index);
        });
        //----------positive------------
        var eventPositive = days_result.map(function (days_result) {
            return days_result[0];
        });
        //-------------neutral--------------
        var eventneutral = days_result.map(function (days_result) {
            return days_result[1];
        });
        //-------------negative--------------
        var eventnegative = days_result.map(function (days_result) {
            return days_result[2];
        });
        var eventPPositive = calPositive(eventPositive,eventneutral,eventnegative,days);
        var eventPositiveData = ['positivefrequency'].concat(eventPPositive);
    //    draw('byEvent', [positive_eventData, neutral_eventData, negative_eventData], "line", days);
        drawPercentage([eventPositiveData], "line", days);
    }

    function byDateAndCitiesPositive(){
        clearAllInterval();
        $('#time').html('');
        var posEventInit = cities.map(function (city) {
            return nextHalfData.by_event[city][0][0];
        });
        var neuEventtInit = cities.map(function (city) {
            return nextHalfData.by_event[city][0][1];
        });
        var negEventInit = cities.map(function (city) {
            return nextHalfData.by_event[city][0][2];
        });
        var initEventPositive = calPositive(posEventInit,neuEventtInit,negEventInit,cities);
        var initEventPositiveData = ['positivefrequency'].concat(initEventPositive);
        var chart_byEvent_interval = drawPercentageWithGrid([initEventPositiveData], cities);

        var dayIndex = 1;
        document.getElementById('time').innerText = days[0];
        var timeInterval = setInterval(function () {
            if (dayIndex === days.length) {
                dayIndex = 0;
            }
            var pos = cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][0];
            });

            var neut = cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][1];
            });

            var neg = cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][2];
            });

            var changedEventPositive = calPositive(pos,neut,neg,cities);
            var changedEventPositiveData = ['positivefrequency'].concat(changedEventPositive);
            chart_byEvent_interval.load({
                columns: [changedEventPositiveData]
            });

            document.getElementById('time').innerText = days[dayIndex];
            dayIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);
    }


    function byDate() {
        clearAllInterval();
        $('#time').html('');
        var days_result = days.map(function (days, index) {
            return getSum(nextHalfData.by_event, index);
        });
        //----------positive------------
        var eventPositive = days_result.map(function (days_result) {
            return days_result[0];
        });
        var positive_eventData = ['positive'].concat(eventPositive);
        //-------------neutral--------------
        var eventneutral = days_result.map(function (days_result) {
            return days_result[1];
        });
        var neutral_eventData = ['neutral'].concat(eventneutral);
        //-------------negative--------------
        var eventnegative = days_result.map(function (days_result) {
            return days_result[2];
        });
        var negative_eventData = ['negative'].concat(eventnegative);
        draw([positive_eventData, neutral_eventData, negative_eventData], "line", days);
    }

    function byDateAndCities() {
        clearAllInterval();
        $('#time').html('');
        var posEventInit = ['positive'].concat(cities.map(function (city) {
            return nextHalfData.by_event[city][0][0];
        }));
        var neuEventtInit = ['neutral'].concat(cities.map(function (city) {
            return nextHalfData.by_event[city][0][1];
        }));
        var negEventInit = ['negative'].concat(cities.map(function (city) {
            return nextHalfData.by_event[city][0][2];
        }));
        var chart_byEvent_interval = drawWithGrid([posEventInit, neuEventtInit, negEventInit], cities, 8000);

        var dayIndex = 1;
        document.getElementById('time').innerText = days[0];
        var timeInterval = setInterval(function () {
            if (dayIndex === days.length) {
                dayIndex = 0;
            }
            var pos = ['positive'].concat(cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][0];
            }));

            var neut = ['neutral'].concat(cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][1];
            }));

            var neg = ['negative'].concat(cities.map(function (city) {
                return nextHalfData.by_event[city][dayIndex][2];
            }));

            chart_byEvent_interval.load({
                columns: [
                    pos, neut, neg
                ]
            });

            document.getElementById('time').innerText = days[dayIndex];
            dayIndex++;
        }, 1500);
        timeIntervalList.push(timeInterval);

    }
}

function getClusterResultInit() {
    $('#get_cluster_result').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_cluster_result').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-5"><select class="form-control" id="availableClusterDateInterval"></select></div>' +
            '<div class="col-sm-5 col-sm-offset-1"><select class="form-control" id="availableClusterCities"></select></div>' +
            '<div class="col-sm-12 clusterContainer"></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-5"><select class="form-control" id="availableClusterDateInterval_M"></select></div>' +
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').addClass('text-center');
        $('#apiGraph').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        loadMap();
        loadClusterAvailableDateInterval();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function getGeneralWordcloudResultInit() {
    $('#get_general_wordcloud_result').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_general_wordcloud_result').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval"></select></div>' +
            '<div class="col-sm-5 col-sm-offset-1"><select class="form-control" id="availableCities"></select></div>' +
            '<div class="col-sm-12 clusterContainer"></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_M"></select></div>' +
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').addClass('text-center');
        $('#apiGraph').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        loadMap();
        loadWordCloudAvailableDateInterval('general');
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function getHashtagWordcloudResultInit() {
    $('#get_hashtag_wordcloud_result').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_hashtag_wordcloud_result').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval"></select></div>' +
            '<div class="col-sm-5 col-sm-offset-1"><select class="form-control" id="availableCities"></select></div>' +
            '<div class="col-sm-12 clusterContainer"></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_M"></select></div>' +
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').addClass('text-center');
        $('#apiGraph').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        loadMap();
        loadWordCloudAvailableDateInterval('hash_tag');
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function getLangResultInit() {
    $('#get_lang_result').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_lang_result').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval"></select></div>' +
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>City</td><td>Language</td><td>Percentage</td></tr></table></div>'
        );
        $('#apiMap').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_M"></select></div>' +
            '<div class="col-sm-12" style="height:100%;margin-top:20px;"><div id="map"></div></div>'
        );
        $('#apiGraph').html(
            '<div class="col-sm-5"><select class="form-control" id="availableDateInterval_G"></select></div>' +
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        loadMap();
        loadLangAvailableDateInterval();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function getSportsLangInit() {
    $('#get_sports_lang').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sports_lang').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>Sports</td><td>Language</td><td>Percentage</td></tr></table></div>'
        );
        $('#apiMap').addClass('text-center');
        $('#apiMap').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        $('#apiGraph').html(
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        loadSportLang();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function getSportsSentimentInit() {
    $('#get_sports_sentiment').on('click', function () {
        for (var t in tabs) {
            $('#' + tabs[t]).css({
                'display': 'block'
            });
        }
        $('.senariosList').children().removeClass('active');
        $('#get_sports_sentiment').addClass('active');
        $('#apiList').html(
            '<div class="col-sm-8 col-sm-offset-2" style="margin-top:20px;color:white"><table class="table numDataList"><tr><td>Sports</td><td>Sentiment Value</td></tr></table></div>' +
            '<div class="col-sm-12 text-center" style="color:white"><h5>Closer to -1 means more Nagetive.</h5><h5>0 is Neutral</h5><h5>Closer to 1 means more Positive.</h5></div>'
        );
        $('#apiMap').addClass('text-center');
        $('#apiMap').html('<h3 style="color:white">Not Available for This Scenario.</h3>');
        $('#apiGraph').html(
            '<div class="col-sm-12 text-center" id="chart"></div>'
        );
        loadSportSentiment();
        for (var tt in tabs) {
            if ($('.navTab.active').attr('value') === tabs[tt]) {
                $('#' + tabs[tt]).css({
                    'display': 'block'
                });
            } else {
                $('#' + tabs[tt]).css({
                    'display': 'none'
                });
            }
        }
    });
}

function loadSportLang() {
    var sportsCategory = [];
    $.getJSON(host + 'get_sports_lang', function(data) {
        var newData = [];
        data.forEach(function(e) {
            var sportName = Object.keys(e)[0];
            sportsCategory.push(sportName);
            var languages = Object.keys(e[sportName]);
            var eng = 0;
            var non_eng = 0;
            for (var i in languages) {
                if (languages[i] == 'en') {
                    eng += e[sportName][languages[i]];
                } else {
                    non_eng += e[sportName][languages[i]];
                }
            }
            newData.push({
                'eng': eng,
                'non_eng': non_eng,
                'sportName':sportName,
                'engP':eng*100/(eng+non_eng),
                'non_engP':non_eng*100/(eng+non_eng),
            });
        });
        tempData = newData;
        loadList();
        loadGraph();
    });

    function loadList() {
        $('.numDataList').html('<tr><td>Type Of Sports</td><td>Language</td><td>Number Of Posts</td></tr>');
        tempData = tempData.sort(function(a,b){
            return b.eng-a.eng;
        });
        for (var i in tempData) {
            $('.numDataList').append('<tr><td>' + tempData[i].sportName + '</td><td>English</td><td class="text-center">' + tempData[i].eng + '</td></tr>');
            $('.numDataList').append('<tr><td></td><td>No English</td><td class="text-center">' + tempData[i].non_eng +'</td></tr>');
        }
    }

    function loadGraph() {
        tempData = tempData.sort(function(a,b){
            return b.engP-a.engP;
        });

        var sportName = [];
        var data = [
            ['English', 'No English']
        ];

        for(var i in tempData){
            sportName.push(tempData[i].sportName);
            data.push([tempData[i].engP.toFixed(2),tempData[i].non_engP.toFixed(2)]);
        }

        loadChart(data, 'bar', sportName, 0, true,['English','No English']);
    }

}

function loadSportSentiment() {
    $('.numDataList').html('<tr><td>Sports</td><td>Sentiment Value</td></tr>');
    var newData = [['Sentiment Value']];
    $.getJSON(host + 'get_sports_sentiment', function (data) {
        var keys = Object.keys(data);
        for (var i in keys) {
            $('.numDataList').append('<tr><td>' + keys[i] + '</td><td>' + data[keys[i]].toFixed(4) + '</td></tr>');
            newData.push([data[keys[i]].toFixed(4)]);
        }
        loadChart(newData, 'bar', keys, 60, true,[]);
    });
}

function loadLangAvailableDateInterval() {
    $.getJSON(host + 'get_lang_available_date_interval', function (data) {
        $('#availableDateInterval').html('');
        $('#availableDateInterval_M').html('');
        $('#availableDateInterval_G').html('');
        for (var i=data.date_intervals.length-1;i>=0;i--) {
            $('#availableDateInterval').append(
                '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][
                    1
                    ] + '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
            );
            $('#availableDateInterval_M').append(
                '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][
                    1
                    ] +
                '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
            );
            $('#availableDateInterval_G').append(
                '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][
                    1
                    ] +
                '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
            );
        }
        loadLangList();
        loadLangMap();
        loadLangGraph();
    });

    $('#availableDateInterval').on('change', function () {
        loadLangList();
    });

    $('#availableDateInterval_M').on('change', function () {
        loadLangMap();
    });

    $('#availableDateInterval_G').on('change', function () {
        loadLangGraph();
    });


    function loadLangList() {
        var startValue = $('#availableDateInterval').find('option:selected').attr('value1');
        var endValue = $('#availableDateInterval').find('option:selected').attr('value2');
        $('.numDataList').html('<tr><td>City</td><td>Language</td><td>Percentage</td></tr>');
        $.getJSON(host + 'get_lang_result/' + startValue + '/' + endValue, function (data) {
            var cities = Object.keys(data.result);
            for (var i in cities) {
                $('.numDataList').append('<tr><td>' + cities[i] + '</td><td>English</td><td>' + (data.result[
                        cities[i]].eng * 100 / (data.result[cities[i]].eng + data.result[cities[i]].non_eng))
                        .toFixed(
                            2) + '%</td></tr>');
                $('.numDataList').append('<tr><td></td><td>No English</td><td>' + (data.result[
                        cities[i]].non_eng * 100 / (data.result[cities[i]].eng + data.result[cities[i]]
                        .non_eng))
                        .toFixed(
                            2) + '%</td></tr>');
            }
        });
    }

    function loadLangMap() {
        var startValue = $('#availableDateInterval_M').find('option:selected').attr('value1');
        var endValue = $('#availableDateInterval_M').find('option:selected').attr('value2');
        if (mapMarkers.length !== 0) {
            for (var i in mapMarkers) {
                mapInstance.removeLayer(mapMarkers[i]);
            }
            mapMarkers = [];
        }
        $.getJSON(host + 'get_lang_result/' + startValue + '/' + endValue, function (data) {
            var cities = Object.keys(data.result);
            for (var i in cities) {
                var oneCircleMarker = L.circleMarker([settings.cities[cities[i]].lat, settings.cities[
                    cities[i]].lon], {
                    stroke: false,
                    fillColor: 'red',
                    fillOpacity: 0.7,
                    radius: 10,
                });
                var msg = '<h4 class="text-center cusFont">' + cities[i].toUpperCase() + '</h4>';
                msg += '<p>English: ' + (data.result[cities[i]].eng * 100 / (data.result[cities[i]].eng + data.result[
                        cities[i]].non_eng)).toFixed(2) + '%</p>';
                msg += '<p>No English: ' + (data.result[cities[i]].non_eng * 100 / (data.result[cities[i]].eng +
                    data.result[cities[i]].non_eng)).toFixed(2) + '%</p>';
                oneCircleMarker.bindPopup(msg, {
                    closeButton: false,
                });
                oneCircleMarker.on('mouseover', function (e) {
                    this.openPopup();
                });
                mapInstance.addLayer(oneCircleMarker);
                mapMarkers.push(oneCircleMarker);
            }
        });
    }

    function loadLangGraph() {
        var startValue = $('#availableDateInterval_G').find('option:selected').attr('value1');
        var endValue = $('#availableDateInterval_G').find('option:selected').attr('value2');
        var returnResult = [
            ['English', 'No English']
        ];
        var cities = [];
        $.getJSON(host + 'get_lang_result/' + startValue + '/' + endValue, function (data) {
            cities = Object.keys(data.result);
            for (var i in cities) {
                returnResult.push(
                    [
                        (data.result[cities[i]].eng * 100 / (data.result[cities[i]].eng + data.result[
                            cities[i]].non_eng)).toFixed(2),
                        (data.result[cities[i]].non_eng * 100 / (data.result[cities[i]].eng + data.result[
                            cities[i]].non_eng)).toFixed(2)
                    ]);
            }
            loadChart(returnResult, 'bar', cities, 60, false,['English', 'No English']);
        });

    }
}

function loadWordCloudAvailableDateInterval(option) {
    $.getJSON(host + 'get_wordcloud_available_date_interval', function (data) {
        $('#availableDateInterval').html('');
        $('#availableDateInterval_M').html('');
        for (var i=data.date_intervals.length-1;i>=0;i--) {
            if (data.date_intervals[i][2] === option) {
                $('#availableDateInterval').append(
                    '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][
                        1
                        ] +
                    '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
                );
                $('#availableDateInterval_M').append(
                    '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][
                        1
                        ] +
                    '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
                );
            }
        }
        loadWordCloudCities(option, false);
        loadWordCloudCities(option, true);
    });

    $('#availableDateInterval').on('change', function () {
        loadWordCloudCities(option, false);
    });

    $('#availableDateInterval_M').on('change', function () {
        loadWordCloudCities(option, true);
    });

    $('#availableCities').on('change', function () {
        updateRanking();
    });

    function loadWordCloudCities(option, isMap) {
        var startValue;
        var endValue;
        if (!isMap) {
            startValue = $('#availableDateInterval').find('option:selected').attr('value1');
            endValue = $('#availableDateInterval').find('option:selected').attr('value2');
            $.getJSON(host + 'get_wordcloud_result/' + startValue + '/' + endValue + '/' + option, function (data) {
                tempData = data.result;
                var cities = Object.keys(data.result);
                $('#availableCities').html('');
                for (var i in cities) {
                    $('#availableCities').append(
                        '<option value="' + cities[i] + '">' + cities[i] + '</option>'
                    );
                }
                updateRanking();
            });
        } else {
            startValue = $('#availableDateInterval_M').find('option:selected').attr('value1');
            endValue = $('#availableDateInterval_M').find('option:selected').attr('value2');
            if (mapMarkers.length !== 0) {
                for (var i in mapMarkers) {
                    mapInstance.removeLayer(mapMarkers[i]);
                }
                mapMarkers = [];
            }
            $.getJSON(host + 'get_wordcloud_result/' + startValue + '/' + endValue + '/' + option, function (data) {
                var cities = Object.keys(data.result);
                for (var j in cities) {
                    var oneCircleMarker = L.circleMarker([settings.cities[cities[j]].lat, settings.cities[
                        cities[j]].lon], {
                        stroke: false,
                        fillColor: 'red',
                        fillOpacity: 0.7,
                        radius: 10,
                    });
                    var tempArr = sortObjToArr(data.result[cities[j]]);
                    var msg = '<h4 class="text-center cusFont">' + cities[j].toUpperCase() + '</h4>';
                    var m;
                    if (tempArr.length < 10) {
                        m = tempArr.length;
                    } else {
                        m = 10;
                    }
                    for (var n = 0; n < m; n++) {
                        msg += '<p>#' + (parseInt(n) + 1) + ': ' + tempArr[n][0] +
                            '</p>';
                    }
                    oneCircleMarker.bindPopup(msg, {
                        closeButton: false,
                    });
                    oneCircleMarker.on('mouseover', function (e) {
                        this.openPopup();
                    });
                    mapInstance.addLayer(oneCircleMarker);
                    mapMarkers.push(oneCircleMarker);
                }
            });
        }
    }

    function updateRanking() {
        var city = $('#availableCities').find('option:selected').attr('value');
        $('.clusterContainer').html('');
        var tempArr = sortObjToArr(tempData[city]);
        var m;
        if (tempArr.length < 10) {
            m = tempArr.length;
        } else {
            m = 10;
        }
        for (var j = 0; j < m; j++) {
            $('.clusterContainer').append(
                '<div class="clusterItems text-center"><div><h4>#' + (parseInt(j) + 1) + '</h4><h4>' + tempArr[j][0] +
                '</h4></div></div>'
            );
        }
    }

    function sortObjToArr(obj) {
        var keys = Object.keys(obj);
        var tempArr = [];
        for (var i in keys) {
            tempArr.push([keys[i], obj[keys[i]]]);
        }
        tempArr = tempArr.sort(function (a, b) {
            return b[1] - a[1];
        });
        return tempArr;
    }
}

function loadClusterAvailableDateInterval() {
    $.getJSON(host + 'get_cluster_available_date_interval', function (data) {
        $('#availableClusterDateInterval').html('');
        $('#availableClusterDateInterval_M').html('');
        for (var i=data.date_intervals.length-1;i>=0;i--) {
            $('#availableClusterDateInterval').append(
                '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][1] +
                '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
            );
            $('#availableClusterDateInterval_M').append(
                '<option value1="' + data.date_intervals[i][0] + '" value2="' + data.date_intervals[i][1] +
                '">' + data.date_intervals[i][0] + ' - ' + data.date_intervals[i][1] + '</option>'
            );
        }
        updateCitiesOption();
        updateCitiesOption('map');
    });

    $('#availableClusterDateInterval').on('change', function (e) {
        updateCitiesOption();
    });

    $('#availableClusterDateInterval_M').on('change', function (e) {
        updateCitiesOption('map');
    });

    $('#availableClusterCities').on('change', function (e) {
        updateClusters();
    });

    function updateCitiesOption(option) {
        var startValue;
        var endValue;
        if (option != 'map') {
            startValue = $('#availableClusterDateInterval').find('option:selected').attr('value1');
            endValue = $('#availableClusterDateInterval').find('option:selected').attr('value2');
            $.getJSON(host + 'get_cluster_result/' + startValue + '/' + endValue, function (data) {
                tempData = data.result;
                var cities = Object.keys(data.result);
                $('#availableClusterCities').html('');
                for (var i in cities) {
                    $('#availableClusterCities').append(
                        '<option value="' + cities[i] + '">' + cities[i] + '</option>'
                    );
                }
                updateClusters();
            });
        } else {
            startValue = $('#availableClusterDateInterval_M').find('option:selected').attr('value1');
            endValue = $('#availableClusterDateInterval_M').find('option:selected').attr('value2');
            if (mapMarkers.length !== 0) {
                for (var i in mapMarkers) {
                    mapInstance.removeLayer(mapMarkers[i]);

                }
                mapMarkers = [];
            }
            $.getJSON(host + 'get_cluster_result/' + startValue + '/' + endValue, function (data) {
                var cities = Object.keys(data.result);
                for (var j in cities) {
                    var oneCircleMarker = L.circleMarker([settings.cities[cities[j]].lat, settings.cities[
                        cities[j]].lon], {
                        stroke: false,
                        fillColor: 'red',
                        fillOpacity: 0.7,
                        radius: 10,
                    });
                    var msg = '<h4 class="text-center cusFont">' + cities[j].toUpperCase() + '</h4>';
                    for (var n in data.result[cities[j]]) {
                        msg += '<p>Cluster' + (parseInt(n) + 1) + ': ' + data.result[cities[j]][n].toString() +
                            '</p>';
                    }
                    oneCircleMarker.bindPopup(msg, {
                        closeButton: false,
                    });
                    oneCircleMarker.on('mouseover', function (e) {
                        this.openPopup();
                    });
                    mapInstance.addLayer(oneCircleMarker);
                    mapMarkers.push(oneCircleMarker);
                }
            });
        }
    }

    function updateClusters() {
        var city = $('#availableClusterCities').find('option:selected').attr('value');
        $('.clusterContainer').html('');
        for (var i in tempData[city]) {
            var word = '';
            for (var j in tempData[city][i]) {
                if (j == tempData[city][i].length - 1) {
                    word += tempData[city][i][j];
                } else {
                    word += tempData[city][i][j] + ' , ';
                }
            }
            $('.clusterContainer').append(
                '<div class="clusterItems text-center"><div><h4>' + word + '</h4></div></div>'
            );
        }
    }
}

function loadMap() {
    mapInstance = new L.Map('map', {
        scrollWheelZoom: false,
    });
    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {
        minZoom: 4,
        maxZoom: 4,
        attribution: osmAttrib
    });
    mapInstance.addLayer(osm);
    mapInstance.setView([-26.224133, 133.718493], 4);
}

function loadChart(data, type, category, degree, rotated,groups) {
    var chart = c3.generate({
        bindto: '#chart',
        data: {
            rows: data,
            type: type,
            groups:[groups],
        },
        padding: {
            top: 20,
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: category,
                tick: {
                    rotate: degree,
                    multiline: false
                },
                height: 70
            },
            rotated: rotated,
        }
    });
}

function draw(columns, chartType, categories) {
    c3.generate({
        bindto: '#chart',
        data: {
            columns: columns,
            type: chartType
        },
        padding: {
            top: 20,
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: categories,
                tick: {
                    rotate: -55,
                    multiline: false
                },
                height: 70
            }

        }
    });
}

function drawWithGrid(columns, categories, maxY) {
    return c3.generate({
        bindto: '#chart',
        data: {
            columns: columns,
            type:'bar',
            groups: columns
        },
        padding: {
            top: 20,
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: categories,
                tick: {
                    rotate: -55,
                    multiline: false
                },
                height: 70
            },
            y: {
                max: maxY
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });
}
function calPositiveByCities(res) {
    var posList=[];
    for(var i = 0; i <res.length;i++){
        var pos= res[i][0];
        var neu= res[i][1];
        var neg= res[i][2];
        posList.push(pos/(pos+neu+neg));
    }
    console.log(posList);
    return posList;
}

function getSum(res, index) {
    return Object.keys(res).reduce(function (acc, key) {
        var points = res[key][index];
        return [acc[0] + points[0], acc[1] + points[1], acc[2] + points[2]];
    }, [0, 0, 0]);
}
function calPositive(pos, ner, neg, res) {
    var total = [];
    for (var i = 0; i < res.length; i++) {
        total.push(pos[i] + ner[i] + neg[i])
    }
    var frequency = [];
    for (var j = 0; j < total.length; j++) {
        frequency.push(pos[j] / total[j]);
    }
    return frequency;
}

function drawPercentage(columns, chartType, categories) {
    return c3.generate({
        bindto: '#chart',
        data: {
            columns: columns,
            type: chartType
        },
        padding: {
            top: 20
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: categories,
                tick: {
                    rotate: -55,
                    multiline: false
                },
                height: 70
            },
            y: {
                // min: 0.1,
                // max: 1,
                tick: {
                    format: d3.format('%')
                }
            }

        }
    });
}
function drawPercentageWithGrid(columns, categories) {
    return c3.generate({
        bindto: '#chart',
        data: {
            type:'bar',
            columns: columns
        },
        padding: {
            top: 20,
        },
        size: {
            width: 600,
            height: 550
        },
        axis: {
            x: {
                type: 'category',
                categories: categories,
                tick: {
                    rotate: -55,
                    multiline: false
                },
                height: 70
            },
            y: {
                // min: 0.1,
                tick: {
                    format: d3.format('%')
                }
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });
}
