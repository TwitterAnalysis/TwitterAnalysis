import couchdb
import json
import sys
from flask import Flask, render_template, jsonify,json
import os
from couchdb.design import ViewDefinition
from flask_cors import CORS, cross_origin
app = Flask(__name__)
CORS(app)
db_url = ''

if not os.path.exists('../config.conf'):
    sys.exit(1)
with open('../config.conf') as f:
    config = f.readline()
    db_url = config.split('=')[1].strip()
server = couchdb.Server(db_url)
try:
    cluster_db = server.create('cluster_result')
    wordcloud_db=server.create('word_cloud_result')
    lang_db=server.create('lang_result')
except couchdb.http.PreconditionFailed:
    cluster_db = server['cluster_result']
    wordcloud_db = server['word_cloud_result']
    lang_db = server['lang_result']


# Home Page
@app.route('/')
def show_home():
    return render_template("index.html")


# Sentiment API Readme File
@app.route('/sentiment/readme/')
def show_sentiment_readme():
    return render_template("home.html")


# Read JSON File
def read_json(file_name):
    with open(file_name, 'r') as f:
        data = json.load(f)
    return data


# Sentimental Analysis
# By City
# Twitter Data
@app.route('/sentiment/by_city/')
def get_sentiment_city():
    file = "./static/sentiment_by_city.json"
    return jsonify(read_json(file_name=file))


# AURIN Unemployment Benefit Data
@app.route('/sentiment/by_city/aurin/')
def get_sentiment_aurin():
    file = "./static/sentiment_aurin_benefit.json"
    return jsonify(read_json(file_name=file))


# By Time
@app.route('/sentiment/by_time/')
def get_sentiment_time():
    file = "./static/sentiment_by_time.json"
    return jsonify(read_json(file_name=file))


# By Weekday
@app.route('/sentiment/by_weekday/')
def get_sentiment_weekday():
    file = "./static/sentiment_by_weekday.json"
    return jsonify(read_json(file_name=file))


# By Season
@app.route('/sentiment/by_season/')
def get_sentiment_season():
    file = "./static/sentiment_by_season.json"
    return jsonify(read_json(file_name=file))


# By Event
@app.route('/sentiment/by_event/')
def get_sentiment_event():
    file = "./static/sentiment_by_event.json"
    return jsonify(read_json(file_name=file))


@app.route('/get_cluster_result/<start_date>/<end_date>')
def get_cluster_result(start_date, end_date):
    doc = None
    try:
        doc = cluster_db[('%s_%s' % (start_date, end_date)).replace('-', '_')]
        return json.dumps(doc)
    except couchdb.http.ResourceNotFound:
        return json.dumps({'result': doc})


@app.route('/get_wordcloud_result/<start_date>/<end_date>/<mode>')
def get_wordcloud_result(start_date, end_date, mode):
    doc = None
    try:
        key = ('%s_%s_%s' % (start_date, end_date, mode)).replace('-', '_')
        doc = wordcloud_db[key]
        return json.dumps(doc)
    except couchdb.http.ResourceNotFound:
        return json.dumps({'result': doc})


@app.route('/get_lang_result/<start_date>/<end_date>')
def get_lang_result(start_date, end_date):
    doc = None
    try:
        doc = lang_db[('%s_%s' % (start_date, end_date)).replace('-', '_')]
        return json.dumps(doc)
    except couchdb.http.ResourceNotFound:
        return json.dumps({'result': doc})


@app.route('/get_cluster_available_date_interval')
def get_cluster_available_date_interval():
    dates = []
    try:
        for d in cluster_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})
    except couchdb.http.ResourceNotFound:
        map_func = '''
               function(doc){
                   emit(doc._id,[doc.start_date,doc.end_date]);
               }
           '''
        view = ViewDefinition('result', 'get_all_available_dates', map_fun=map_func)
        view.sync(cluster_db)
        for d in cluster_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})


@app.route('/get_wordcloud_available_date_interval')
def get_wordcloud_available_date_interval():
    dates = []
    try:
        for d in wordcloud_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})
    except couchdb.http.ResourceNotFound:
        map_func = '''
                   function(doc){
                       emit(doc._id,[doc.start_date,doc.end_date,doc.mode]);
                   }
               '''
        view = ViewDefinition('result', 'get_all_available_dates', map_fun=map_func)
        view.sync(wordcloud_db)
        for d in wordcloud_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})


@app.route('/get_lang_available_date_interval')
def get_lang_available_date_interval():
    dates = []
    try:
        for d in lang_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})
    except couchdb.http.ResourceNotFound:
        map_func = '''
                   function(doc){
                       emit(doc._id,[doc.start_date,doc.end_date]);
                   }
               '''
        view = ViewDefinition('result', 'get_all_available_dates', map_fun=map_func)
        view.sync(lang_db)
        for d in lang_db.view('result/get_all_available_dates'):
            dates.append(d.value)
        return json.dumps({'date_intervals': dates})


@app.route('/get_sports_sentiment')
def get_sports_sentiment():
    if os.path.exists('./static/sports_sentiment.json'):
        f = open('./static/sports_sentiment.json', 'r')
        return f.read()
    else:
        return json.dumps({'result': None})


@app.route('/get_sports_lang')
def get_sports_lang():
    if os.path.exists('./static/sports_lang.json'):
        f = open('./static/sports_lang.json', 'r')
        return f.read()
    else:
        return json.dumps({'result': None})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True,threaded=True)
