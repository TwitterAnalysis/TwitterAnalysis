# -*- coding:utf-8 -*-
import datetime
import re
import string
import time

import couchdb
import langdetect
from couchdb.design import ViewDefinition
import nltk
import pandas as pd
from couchdb.design import ViewDefinition
from gensim.models import Word2Vec
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from textblob import TextBlob
from langdetect import detect, DetectorFactory

DetectorFactory.seed = 0

def get_all_tweets(db):
    get_tweets = 'function(doc) { emit(doc.id_str, [doc.id_str,doc.text,doc.created_at,doc.place.name]); }'
    view = ViewDefinition('twitter', 'get_tweets', get_tweets)
    view.sync(db)
    tweets = []
    for doc in db.view('twitter/get_tweets'):
        tweets.append({'id': doc.value[0], 'text': doc.value[1], 'time': doc.value[2], 'place': doc.value[3]})
    return tweets


def get_tweets_between_date(startdate, enddate, db):
    func = '''
		function(doc){
			emit(Date.parse(doc.created_at),[doc.id_str,doc.text,doc.created_at]);
		}
	'''
    view = couchdb.design.ViewDefinition('twitter', 'get_tweets_between_date', func)
    view.sync(db)
    tweets = []
    for doc in db.view('twitter/get_tweets_between_date', start_key=int(time.mktime(startdate.timetuple()) * 1000), end_key=int(time.mktime(enddate.timetuple()) * 1000)):
        tweets.append({'id': doc.value[0], 'text': doc.value[1], 'time': doc.value[2]})
    return tweets

def tokenize_only(text):
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    return filtered_tokens


def tokenize_and_remove_stopwords(text):
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.append('would')
    stopwords.append('kmh')
    stopwords.append('mph')
    stopwords.append('u')
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    regex = re.compile(r'[^A-Za-z0-9]')
    for token in tokens:
        if re.search('[a-zA-Z0-9]', token) and token.lower() not in stopwords:
            filtered_tokens.append(regex.sub('', token))
    return filtered_tokens


def tokenize_and_keep_nns_and_remove_stopwords(text):
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.append('would')
    stopwords.append('kmh')
    stopwords.append('mph')
    stopwords.append('u')
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    regex = re.compile(r'[^A-Za-z0-9]')
    for token in tokens:
        if re.search('[a-zA-Z]', token) and token.strip().lower() not in stopwords:
            if len(token) < 3:
                continue
            tag = TextBlob(token).pos_tags[0][1]
            if re.match(r'NN.*', tag):
                filtered_tokens.append(regex.sub('', token.strip()))
    return filtered_tokens


def tokenize_and_keep_nns_and_extract_np_and_remove_stopwords(text):
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.append('would')
    stopwords.append('kmh')
    stopwords.append('mph')
    stopwords.append('u')
    tokens = [word[0] for sent in nltk.sent_tokenize(text) for word in TextBlob(sent).pos_tags if (word[1] == 'NN' or word[1] == 'NNP') and word[0].lower() not in stopwords]
    nps=list(TextBlob(text).noun_phrases)
    lower_tokens= list(map(lambda x: x.lower(), tokens))
    tokens.extend([np for np in nps if np.lower() not in lower_tokens and len(nltk.word_tokenize(np))<5])
    return tokens


def strip_proppers(text):
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent) if word.islower()]
    return "".join([" " + i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()


def text_pre_process_with_hash_tag(doc_df):
    regex = re.compile(r'[^A-Za-z0-9\s\#\.\,]|(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?')
    return list(map(lambda x: regex.sub('', x.strip().replace('&amp', '').replace('can\'t', 'can not').replace('\'m', ' am').replace('\'re', ' are').replace('\'s', ' is').replace('\'ve', ' have').replace('n\'t', ' not').replace('\'d', ' would').replace('\'ll', ' will')), list(doc_df.text)))


def text_pre_process(doc_df):
    regex = re.compile(r'[^A-Za-z0-9\s\.\,\-]|(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?')
    return list(map(lambda x: regex.sub('', x.strip().replace('&amp', '').replace('can\'t', 'can not').replace('\'m', ' am').replace('\'re', ' are').replace('\'s', ' is').replace('\'ve', ' have').replace('n\'t', ' not').replace('\'d', ' would').replace('\'ll', ' will')), list(doc_df.text)))

def text_pre_process_for_lang(doc_df):
    regex=re.compile(r'[^\w\s\.\,\-]|(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?')
    return list(map(lambda x: regex.sub('',x.strip().replace('&amp','')),list(doc_df.text)))

def cluster_with_kmeans(doc_df, num_topics, num_words, pre_process=text_pre_process, vectorizer_type='tfidf', df=(0.95, 0.05)):
    data = pre_process(doc_df)
    if vectorizer_type == 'tfidf':
        vectorizer = TfidfVectorizer(max_df=df[0], min_df=df[1], ngram_range=(1, 3), stop_words='english', use_idf=True, lowercase=True, tokenizer=tokenize_and_remove_stopwords)
    else:
        vectorizer = CountVectorizer(stop_words='english', ngram_range=(1, 3), lowercase=True, max_df=df[0], min_df=df[1], tokenizer=tokenize_and_remove_stopwords)
    matrix = vectorizer.fit_transform(data)
    terms = vectorizer.get_feature_names()
    km = KMeans(n_clusters=num_topics)
    km.fit(matrix)
    clusters = km.labels_.tolist()
    doc_df['cluster'] = clusters
    cluster_words = []
    order_centroids = km.cluster_centers_.argsort()[:, ::-1]
    totalvocab_tokenized = []
    for i in data:
        allwords_tokenized = tokenize_and_remove_stopwords(i)
        totalvocab_tokenized.extend(allwords_tokenized)
    vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index=totalvocab_tokenized)
    for i in range(num_topics):
        related_words = []
        for ind in order_centroids[i, :num_words]:
            related_words.append(vocab_frame.ix[terms[ind].split(' ')].values.tolist()[0][0])
        cluster_words.append(related_words)
    return cluster_words


def cluster_with_word2vec_and_km(doc_df, num_topics, num_words, pre_process=text_pre_process, tokenize=tokenize_and_keep_nns_and_extract_np_and_remove_stopwords):
    data = pre_process(doc_df)
    sentences = [tokenize(t) for t in data]
    if len(sentences) < 20:
        return []
    # print(sentences)
    model = Word2Vec(sentences, min_count=1)
    word_vectors = model.wv.syn0
    # print(word_vectors.shape[0])
    if word_vectors.shape[0] < num_topics:
        return []
    kmeans_clustering = KMeans(n_clusters=num_topics)
    idx = kmeans_clustering.fit_predict(word_vectors)
    word_centroid_map = dict(zip(model.wv.index2word, idx))
    result = []
    for cluster in range(num_topics):
        words = []
        for i in range(0, len(word_centroid_map.values())):
            if list(word_centroid_map.values())[i] == cluster:
                words.append(list(word_centroid_map.keys())[i])
        result.append(words[:num_words])
    return result


def get_hash_tag_word_cloud(doc_df, pre_process=text_pre_process_with_hash_tag):
    data = pre_process(doc_df)
    hash_tags = {}
    for t in data:
        for h in re.findall(r'#\w+', t):
            if h[1:].strip() not in hash_tags:
                hash_tags[h[1:].strip()] = 0
            hash_tags[h[1:].strip()] += 1
    return hash_tags


def get_general_word_cloud(doc_df, pre_process=text_pre_process,tokenize=tokenize_and_keep_nns_and_remove_stopwords):
    tdict = {}
    try:
        data = pre_process(doc_df)
        min_df=1 if len(data)<20 else 5
        cv_vectorizer = CountVectorizer( stop_words='english', analyzer='word', lowercase=False, max_df=0.9, min_df=min_df, tokenizer=tokenize)
        cv_matrix = cv_vectorizer.fit_transform(data)
        fq = cv_matrix.toarray().sum(axis=0)
        tms = cv_vectorizer.get_feature_names()
        tmp_dict=list(map(lambda x:(x,fq[tms.index(x)]),tms))
        tmp_dict=sorted(tmp_dict,key=lambda x:x[1],reverse=True)[:10]
        for td in tmp_dict:
            tdict[str(td[0])]=int(td[1])
        # for t in tms:
        #     tdict[t] = fq[tms.index(t)]
        return tdict
    except:
        return tdict

def static_language_proportion(doc_df, pre_process=text_pre_process_for_lang):
    data=pre_process(doc_df)
    result={}
    eng_result=0
    non_eng_result=0
    for text in data:
        if len(text) < 30:
            continue
        try:
            lang=detect(text)
            if lang=='en':
                eng_result+=1
            else:
                non_eng_result+=1
        except langdetect.lang_detect_exception.LangDetectException:
            continue
        if lang not in result:
            result[lang]=0
        result[lang]+=1
    return result,eng_result,non_eng_result

if __name__ == '__main__':
    db_url = 'http://admin:admin@115.146.86.74:5000/'
    db_name = 'melbourne'
    # 指定数据库url和数据库名
    server = couchdb.Server(url=db_url)
    db = server[db_name]

    # 用时间获取Tweets, 传入一个datetime的对象
    tweets = get_tweets_between_date(datetime.datetime.strptime('2017-05-08 00:00:00', '%Y-%m-%d %H:%M:%S'), datetime.datetime.strptime('2017-05-09 00:00:00', '%Y-%m-%d %H:%M:%S'), db=db)

    # 获取该数据库所有的tweets
    tweets=get_all_tweets(db)

    # 做成pandas的DataFrame，并去重
    doc_df = pd.DataFrame(tweets).drop_duplicates()

    # 用kmeans聚类，该方法最快，效率高，效果较差。
    # params:设定类别和每个类别的代表词个数,设定向量化方式['count','tfidf]，
    # 设定最大和最小document frenquency，大数据集推荐(0.9,0.1)，小数据集推荐(0.99,0.01)
    # 返回结果是list,[[a,a,a,a],[b,b,b,b]...]
    # print(cluster_with_kmeans(doc_df,5,5,vectorizer_type='count',df=(0.99,0.01)))

    # lda聚类，该方法很慢，效率低，效果较好。
    # params:设定类别个数和每个类别的代表词个数，
    # 建议不要让用户直接调用，可以周期循环，获得结果持久化之后获取
    # print(cluster_with_lda(doc_df,5,5))

    # word2vec&kmeans聚类，效率高，效果较好。
    # params:设定类别个数和类别代表词个数
    # print(cluster_with_word2vec_and_km(doc_df,3,3))

    # 获得hash tag的word cloud数据，结果是字典：{'word1':100,'word2':50}
    # get_hash_tag_word_cloud(doc_df)

    # 获得所有词的word cloud数据，结果是字典：{'word1':100,'word2':50}
    print(get_general_word_cloud(doc_df))
