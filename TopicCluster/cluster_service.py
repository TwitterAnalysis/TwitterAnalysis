# -*- coding:utf-8 -*-
import datetime
import json
import os

import couchdb
import sys

import pandas as pd

from cluster_core import *

city_list = ['sydney', 'melbourne', 'brisbane', 'perth', 'adelaide', 'newcastle', 'gold_coast', 'canberra', 'wollongong', 'sunshine_coast', 'hobart', 'geelong', 'townsville', 'cairns', 'darwin', 'toowoomba', 'ballarat']
dbs = {}
db_url= ''
if not os.path.exists('../config.conf'):
    sys.exit(1)
with open('../config.conf') as f:
    config=f.readline()
    db_url=config.split('=')[1].strip()
server=couchdb.Server(db_url)
for city in city_list:
    dbs[city] = server[city+'_no_repeat']

try:
    cluster_db = server.create('cluster_result')
except couchdb.http.PreconditionFailed:
    cluster_db = server['cluster_result']

try:
    word_cloud_db = server.create('word_cloud_result')
except couchdb.http.PreconditionFailed:
    word_cloud_db = server['word_cloud_result']

try:
    lang_db = server.create('lang_result')
except couchdb.http.PreconditionFailed:
    lang_db = server['lang_result']


def cluster_between_days(start_date, end_date, num_topics, num_words):
    final_result = {}
    for k, v in dbs.items():
        tweets = get_tweets_between_date(startdate=datetime.datetime.strptime(start_date, '%Y-%m-%d'), enddate=datetime.datetime.strptime(end_date, '%Y-%m-%d'), db=v)
        if len(tweets) < 10:
            continue
        doc_df = pd.DataFrame(tweets).drop_duplicates()
        result = cluster_with_word2vec_and_km(doc_df, num_topics=num_topics, num_words=num_words,tokenize=tokenize_and_keep_nns_and_extract_np_and_remove_stopwords)
        if len(result) > 0:
            final_result[k] = result
    if len(list(final_result.keys())) > 0:
        print(final_result)
        with open(('../web/static/cluster_%s_%s.json' % (start_date, end_date)).replace('-', '_'), 'w+') as f:
            json.dump(final_result, f)
        try:
            cluster_db[('%s_%s' % (start_date, end_date)).replace('-', '_')] = {'start_date': start_date, 'end_date': end_date, 'result': final_result}
        except couchdb.http.ResourceConflict:
            doc=cluster_db[('%s_%s' % (start_date, end_date)).replace('-', '_')]
            doc['result']=final_result
            cluster_db.save(doc)

def make_word_cloud_between_days(start_date, end_date, mode):
    final_result = {}
    for k, v in dbs.items():
        tweets = get_tweets_between_date(startdate=datetime.datetime.strptime(start_date, '%Y-%m-%d'), enddate=datetime.datetime.strptime(end_date, '%Y-%m-%d'), db=v)
        if len(tweets) < 10:
            continue
        doc_df = pd.DataFrame(tweets).drop_duplicates()
        result = {}
        if mode == 'hash_tag':
            result = get_hash_tag_word_cloud(doc_df)
        elif mode == 'general':
            result = get_general_word_cloud(doc_df)
        else:
            return
        if len(list(result.keys())) > 0:
            final_result[k] = result
    if len(list(final_result.keys())) > 0:
        print(final_result.keys())
        with open(('../web/static/wordcloud_%s_%s_%s.json'%(start_date,end_date,mode)).replace('-','_'), 'w+') as f:
            json.dump(final_result,f)
        try:
            word_cloud_db[('%s_%s_%s' % (start_date, end_date, mode)).replace('-', '_')] = {'start_date': start_date, 'end_date': end_date, 'mode': mode, 'result': final_result}
        except couchdb.http.ResourceConflict:
            doc=word_cloud_db[('%s_%s_%s' % (start_date, end_date, mode)).replace('-', '_')]
            doc['result']=final_result
            word_cloud_db.save(doc)
            
def get_language_proportion(start_date,end_date):
    final_result={}
    DetectorFactory.seed=0
    for k,v in dbs.items():
        tweets = get_tweets_between_date(startdate=datetime.datetime.strptime(start_date, '%Y-%m-%d'), enddate=datetime.datetime.strptime(end_date, '%Y-%m-%d'), db=v)
        if len(tweets)==0:
            continue
        doc_df = pd.DataFrame(tweets).drop_duplicates()
        result= static_language_proportion(doc_df)
        final_result[k]={'details':result[0],'eng':result[1],'non_eng':result[2]}
    if len(list(final_result.keys()))>0:
        print(final_result)
        with open(('../web/static/lang_%s_%s.json' % (start_date, end_date)).replace('-', '_'),'w+') as f:
            json.dump(final_result, f)
        try:
            lang_db[('%s_%s' % (start_date, end_date)).replace('-', '_')] = {'start_date': start_date, 'end_date': end_date, 'result': final_result}
        except couchdb.http.ResourceConflict:
            doc=lang_db[('%s_%s' % (start_date, end_date)).replace('-', '_')]
            doc['result']=final_result
            lang_db.save(doc)


if __name__ == '__main__':
    if sys.argv[1] == 'cluster':
        if sys.argv[2] == 'all_day':
            for y in range(6, 8):
                for m in range(1, 13):
                    for d in range(2, 32):
                        print(y, m, d)
                        if (m == 4 or m == 6 or m == 9 or m == 11) and d == 31:
                            break
                        if m == 2 and d == 29 and y == 7:
                            break
                        if m == 2 and d == 28 and y == 6:
                            break
                        cluster_between_days('201%d-%02d-%02d' % (y, m, d - 1), '201%d-%02d-%02d' % (y, m, d), 3, 3)
        elif sys.argv[2] == 'all_month':
            for y in range(6, 8):
                for m in range(2, 13):
                    print(y, m)
                    cluster_between_days('201%d-%02d-01' % (y, m - 1), '201%d-%02d-01' % (y, m), 5, 5)
        elif re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[2]) and re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[3]):
            cluster_between_days(sys.argv[2], sys.argv[3], int(sys.argv[4]), int(sys.argv[5]))
    elif sys.argv[1] == 'wordcloud':
        if sys.argv[2] == 'all_day':
            for y in range(6, 8):
                for m in range(1, 13):
                    for d in range(2, 32):
                        print(y, m, d)
                        if (m == 4 or m == 6 or m == 9 or m == 11) and d == 31:
                            break
                        if m == 2 and d == 29 and y == 7:
                            break
                        if m == 2 and d == 28 and y == 6:
                            break
                        make_word_cloud_between_days('201%d-%02d-%02d' % (y, m, d - 1), '201%d-%02d-%02d' % (y, m, d), sys.argv[3])
        elif sys.argv[2] == 'all_month':
            for y in range(6, 8):
                for m in range(2, 13):
                    print(y, m)
                    make_word_cloud_between_days('201%d-%02d-01' % (y, m - 1), '201%d-%02d-01' % (y, m), sys.argv[3])
        elif re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[2]) and re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[3]):
            make_word_cloud_between_days(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1]=='lang':
        if sys.argv[2]=='all_day':
            for y in range(6, 8):
                for m in range(1, 13):
                    for d in range(2, 32):
                        print(y, m, d)
                        if (m == 4 or m == 6 or m == 9 or m == 11) and d == 31:
                            break
                        if m == 2 and d == 29 and y == 7:
                            break
                        if m == 2 and d == 28 and y == 6:
                            break
                        get_language_proportion('201%d-%02d-%02d' % (y, m, d - 1), '201%d-%02d-%02d' % (y, m, d))
        elif sys.argv[2] == 'all_month':
            for y in range(6, 8):
                for m in range(2, 13):
                    print(y, m)
                    get_language_proportion('201%d-%02d-01' % (y, m - 1), '201%d-%02d-01' % (y, m))
        elif re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[2]) and re.match(r'\d{4}-\d{2}-\d{2}', sys.argv[3]):
            get_language_proportion(sys.argv[2], sys.argv[3])