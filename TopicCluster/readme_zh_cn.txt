CLUSTER API README

这是一篇关于聚类结果服务REST-API 的readme：

1. 获取聚类所有可拿到结果的日期间隔:
URL: /get_cluster_available_date_interval
Method: GET
Params: None
Response: {
              "date_intervals": [
                  [
                      "2017-02-18",
                      "2017-02-19"
                  ],
                  [
                      "2017-02-19",
                      "2017-02-20"
                  ],
                  [
                      "2017-02-21",
                      "2017-02-22"
                  ],
              ]
          }
解释: list里装的是一个个interval，调用时候会返回每天的和每月的，或者其他时间间隔，可供后续调取结果使用。

2. 获取词云所有可拿到结果的日期间隔:
URL: /get_wordcloud_available_date_interval
Method: GET
Params: None
Response: {
              "date_intervals": [
                  [
                      "2016-07-27",
                      "2016-07-28",
                      "hash_tag"
                  ],
                  [
                      "2016-07-30",
                      "2016-07-31",
                      "general"
                  ],
                  [
                      "2016-09-19",
                      "2016-09-20",
                      "hash_tag"
                  ],
              ]
          }
解释: list里装的是一个个日期和mode pair，调用时候会返回每天的和每月的，或者其他时间间隔。
      mode是按hash_tag画词云还是所有词语画词云的选项，在之后续调取结果时会用到。

3. 获取聚类某一间隔时间的结果：
URL: /get_cluster_result/<start_date>/<end_date>
Method: GET
Params:
     start_date: 起始日期，YYYY-MM-DD 的格式
     end_date: 结束如期，YYYY-MM-DD 的格式
     *间隔要选取之前调用可选日期之中的，否则会没有结果返回
Response: {
              "_id": "2017_04_22_2017_04_23",
              "_rev": "1-9247330725816adf8cb4ca7a26e927b5",
              "end_date": "2017-04-23",
              "start_date": "2017-04-22",
              "result": {
                  "geelong": [
                              [
                                  "OKC",
                                  "Russ",
                                  "get"
                              ],
                              [
                                  "Russell",
                                  "great",
                                  "NewWeedDispensaryNames"
                              ],
                              [
                                  "Geelong",
                                  "us",
                                  "one"
                              ]
                          ],
                  "melbourne": [
                              [
                                  "Melbourne",
                                  "like",
                                  "time"
                              ],
                              [
                                  "TOTALLY",
                                  "GLOBAL",
                                  "TRANSPARENT"
                              ],
                              [
                                  "based",
                                  "GMSequation",
                                  "democratically"
                              ]
                          ]
              }
          }
解释：调用 get_cluster_result/2017-04-22/2017-04-23 会获得22-23号的聚类结果，按天分类默认是一个城市分3类，每类3个代表词，格式如上。
      *按月分类，或其他时间段分类，类别数目会有变化，代表词数目也会有变化，每个类别的代表词数目可能会不同。
      *城市不一定包括所有城市，有些城市如果当天数据量不过会无法计算聚类结果。

4. 获取词云某一间隔时间的结果：
URL: get_wordcloud_result/<start_date>/<end_date>/<mode>
Method: GET
Params:
     start_date: 起始日期，YYYY-MM-DD 的格式
     end_date: 结束如期，YYYY-MM-DD 的格式
     *间隔要选取之前调用可选日期之中的，否则会没有结果返回
     mode: hash_tag|general
     *mode 参照之前调用的可选日期中的mode
Response:{
             "_id": "2017_04_20_2017_04_21_hash_tag",
             "_rev": "1-c4a740ba507e9461f18d2edbcba4efc4",
             "end_date": "2017-04-21",
             "start_date": "2017-04-20",
             "mode": "hash_tag",
             "result": {
              "geelong": {
                         "1": 1,
                         "Change": 1,
                         "CrazyCatLady": 1,
                         "DaddyOFive": 1,
                         "DailyNotesToSelf": 4,
                         "DamianMonkhorst": 1,
                         "DepartureLounge": 2,
                         "DubNation": 1,
                         "ForwardAsOne": 1,
                         "GDFL": 1,
                         "Geelong": 1,
                         "MakingAMurderer": 1,
                         "MathEd": 1,
                         "MrWillis": 2,
                         "NBAPlayoffs2017": 1,
                         "NEvSJ": 3,
                         "NewWeedDispensaryNames": 6,
                         "NickCaveAndTheBadSeeds": 1,
                         "PRcrisis": 1,
                         "Quakes74": 3,
                         "TheHollowMajors": 1,
                         "ThrowbackThursday": 1,
                         "WaitAndSee": 1,
                         "Webbys": 1,
                         "Yr5makerstgc": 2
                         ...
                     },
                     ...
                }
            }
解释： 调用 get_wordcloud_result/2017-04-20/2017-04-21/hash_tag 会调取该间隔当中通过hash_tag画词云的统计数据，格式如上。

5. 获取语言统计某一间隔时间的结果：
URL: get_lang_result/<start_date>/<end_date>
Method: GET
Params:
     start_date: 起始日期，YYYY-MM-DD 的格式
     end_date: 结束如期，YYYY-MM-DD 的格式
     *间隔要选取之前调用可选日期之中的，否则会没有结果返回
Response:{
             "_id": "2017_04_20_2017_04_21",
             "_rev": "1-d8f71fec50bd055eacb79a7291d55b36",
             "end_date": "2017-04-21",
             "result": {
                 "adelaide": {
                     "details": {
                         "af": 6,
                         "ar": 5,
                         "ca": 1,
                         "cy": 3,
                         "da": 2,
                         "de": 4,
                         "en": 1025,
                         "es": 1,
                         "et": 1,
                         "fr": 4,
                         "id": 6,
                         "it": 2,
                         "ja": 1,
                         "ko": 1,
                         "nl": 5,
                         "no": 1,
                         "pl": 1,
                         "pt": 1,
                         "ro": 1,
                         "sl": 1,
                         "so": 7,
                         "sq": 1,
                         "sv": 12,
                         "tl": 8,
                         "ur": 3,
                         "zh-cn": 2
                     },
                     "eng": 1025,
                     "non_eng": 80
                 },
             }
         }
解释： 调用 get_lang_result/2017-04-20/2017-04-21会调取该间隔当中tweets所用语言的统计数据，格式如上。

6.获取体育场所语言分布结果
URL: /get_sports_lang
Method: GET
Response: [
    {
        "SurfLifeSaving": {
            "th": 3,
            "ca": 1,
            "sv": 1,
            "zh-tw": 1,
            "af": 1,
            "en": 329,
            "id": 1,
            "fr": 2,
            "no": 4,
            "de": 6,
            "so": 1,
            "unknow": 14
        }
    },
]
解释： 这个应该不用解释了

7.获取体育场所情感分类结果
URL: /get_sports_sentiment
Method: GET
Response:{
             "Aerobics": 0.6392045455,
             "Athletics": 0.4444444444,
             "Australian Rules Football": 0.696803653,
             "Badminton": 0.754491018,
             "Basketball": 0.7032258065,
             "Boxing": 0.5,
             "Canoe Polo": 0.9411764706,
             "Cricket": 0.6896551724,
             "Cricket (Indoor)": 0.8461538462,
             "Croquet": 0.8166666667,
             "Cycling": 0.6992481203,
             "Dancing": 0.28125,
             "Equestrian": 1.0833333333,
             "Fitness / Gymnasium Workouts": 0.7219178082,
             "Golf": 0.5714285714,
             "Gymnastics": 0.7689243028,
             "Hockey": 0.6666666667,
             "Lawn Bowls": 0.6419354839,
             "Martial Arts": 0.6386397608,
             "Roller Sports - Other": 0.1875,
             "Sailing": 0.6794871795,
             "Skating": 0.649122807,
             "Snooker / Billiards / Pool": 0.4347826087,
             "Soccer": 0.5,
             "Surf Life Saving": 0.6456043956,
             "Swimming": 0.6478873239,
             "Tennis (Outdoor)": 0.6385542169
         }

解释：这个就是全部的结果了。。每种体育场所的情感均值，靠近1是正面，靠近0是中行，靠近-1是负面