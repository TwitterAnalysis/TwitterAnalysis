#!/usr/bin/env bash
cd /home/ubuntu/TwitterAnalysis/TopicCluster/
python3 ./cluster_service.py cluster `date --date='1 day ago' +%Y-%m-%d` `date +%Y-%m-%d` 3 3 > log.cluster.`date --date='1 day ago' +%Y_%m_%d`_`date +%Y_%m_%d`
python3 ./cluster_service.py wordcloud `date --date='1 day ago' +%Y-%m-%d` `date +%Y-%m-%d` hash_tag > log.wordcloud.`date --date='1 day ago' +%Y-%m-%d`_`date +%Y_%m_%d`_hash_tag
python3 ./cluster_service.py wordcloud `date --date='1 day ago' +%Y-%m-%d` `date +%Y-%m-%d` general > log.wordcloud.`date --date='1 day ago' +%Y-%m-%d`_`date +%Y_%m_%d`_general
python3 ./cluster_service.py lang `date --date='1 day ago' +%Y-%m-%d` `date +%Y-%m-%d` > log.lang.`date --date='1 day ago' +%Y_%m_%d`_`date +%Y_%m_%d`